export const SET_USER_DATA = 'SET_USER_DATA';
export const SET_AUTH_STATUS = 'SET_AUTH_STATUS';
export const SET_VALIDATION_ERROR = 'SET_VALIDATION_ERROR';
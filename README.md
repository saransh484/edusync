# Attendance Management System

Welcome to the Attendance Management System repository! This system is designed to simplify attendance tracking for educational institutions. Students can conveniently mark their attendance by scanning a QR code from the admin's page, and both students and administrators have their respective dashboards for tracking attendance metrics.

## Features

### Student Dashboard

- **Scan QR Code:** Students can easily mark their attendance by scanning the QR code available on the admin's page.
- **Attendance Metrics:** View personal attendance metrics, including the number of classes attended, total classes, and attendance percentage.

### Admin Dashboard

- **Today's Attendance:** Monitor the attendance for the current day, including the total number of students present and absent.
- **Total Students:** Track the overall number of registered students.
- **Attendance Metrics:** View comprehensive metrics, such as the overall attendance percentage and other relevant statistics.

## Get the Docker Images From here:

- [Docker Images](https://drive.google.com/drive/folders/145_OgldbPeV-kcARuAQlAicRgmlJOy9X?usp=sharing)

## Run Images:

- **Load Docker Images**
  
  ```bash
  docker image load frontend_image.tar.gz
  docker image load backend_image.tar.gz
  ```

- **Run the images:**
  
  - For Backend:
    
    ```bash
    docker run -dp 127.0.0.1:5000:5000 backend
    ```
  
  - For Frontend:
    
    ```bash
    docker run -dp 127.0.0.1:3000:3000 frontend
    ```

## Getting Started

1. **Clone the Repository:**
   
   ```bash
   git clone https://github.com/saransh484/Flipr-Attendence-Task.git
   cd Flipr-Attendence-Task
   ```

2. **Setup Database:**
   
   - Create a SQL database to store student and attendance information.

3. **Configuration:**
   
   - Configure the database connection in the `db.js` file.

4. **Install Dependencies:**
   
   ```bash
   npm install
   ```

5. **Run the Application (For Backend):**
   
   ```bash
   npm run dev
   ```

6. **Run the Application (For Frontend):**
   
   ```bash
   npm start
   ```
   
   The application will be accessible at [http://localhost:3000](http://localhost:3000).

## Screenshots

Include screenshots of the student and admin dashboards for a quick overview.

### Student Dashboard

![Student Dashboard](./Screenshots/student.png)

### Admin Dashboard

![Admin Dashboard](./Screenshots/admin.png)

## Contributing

Feel free to contribute to the project by opening issues, providing feedback, or submitting pull requests. Your contributions are highly appreciated!

## License

This project is licensed under the [MIT License](LICENSE).

---

Feel free to customize this README to suit your project's specific details.

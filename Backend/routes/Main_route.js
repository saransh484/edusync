const express = require('express');
const { test, resetPasswordRequest, resetPassword } = require('../controllers/login_controller');
const verifyToken = require('../middleware/authMiddleware');
const { total_students, attendance, total_present, total_absent, allStudents, updateStudent, deleteStudent, searchStudent, dayAttendees } = require('../controllers/admin_dashboard');
const { totalAttendanceOfStudent, student_present, student_absent } = require('../controllers/student_dashboard');
const router = express.Router();

router.get('/test', test)
router.get('/test2',verifyToken,test)

// ----- Admin -----
router.post('/mark_attendance', attendance)

router.get('/total_students', total_students)
router.post('/total_present', total_present)
router.post('/total_absent', total_absent )

router.get('/all_students', allStudents)
router.post('/update_student', updateStudent)
router.post('/delete_student', deleteStudent)
router.post('/search_student', searchStudent)
router.post('/day_attendance', dayAttendees)


router.post('/email',resetPasswordRequest)
router.post('/reset_password',resetPassword)

// ---- Student -----

router.post('/total_attendance', totalAttendanceOfStudent)
router.post('/present_student', student_present)
router.post('/absent_student', student_absent)


module.exports = router
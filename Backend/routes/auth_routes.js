const express = require('express');
const { test, admin_signup, student_signup, student_login, admin_login, refresh, resetPasswordRequest, resetPassword } = require('../controllers/login_controller');
const verifyToken = require('../middleware/authMiddleware');
const adminTokenVerify = require('../middleware/adminAuth')
const router = express.Router();


router.get('/test', adminTokenVerify,test)
router.get('/test2',verifyToken,test)
router.post('/signup', admin_signup)
router.post('/student_signup', student_signup)
router.post('/student_login', student_login)
router.post('/admin_login', admin_login)
router.post('/refresh',refresh)
router.post('/email',resetPasswordRequest)
router.post('/reset_password',resetPassword)


module.exports = router
const mysql = require("mysql");
const cron = require("node-cron");
const moment = require('moment-timezone');

moment.tz.setDefault('Asia/Kolkata');


const db = mysql.createConnection({
    host: process.env.DB_ENDPOINT,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    port: 3306
});

db.connect((err) => {
    if (err) {
        console.error('Error connecting to MySQL:', err);
        return;
    }
    console.log('🐬 Connected to MySQL');

// Create tables if doesn't exist
    const createStudentTableQuery = `
    CREATE TABLE IF NOT EXISTS students (
        roll_no INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        student_id INT UNIQUE NOT NULL,
        name VARCHAR(255) NOT NULL,
        email VARCHAR(255) NOT NULL,
        mobile_number INT NOT NULL,
        hashed_password VARCHAR(255) NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
    )`;
    
    const createAdminTableQuery = `CREATE TABLE IF NOT EXISTS admin (
        admin_id INT PRIMARY KEY,
        name VARCHAR(255) NOT NULL,
        email VARCHAR(255) NOT NULL,
        mobile_number VARCHAR(255) NOT NULL,
        hashed_password VARCHAR(255) NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
    )`;

    const createAttendanceTableQuery = `CREATE TABLE IF NOT EXISTS attendance(
        attendance_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        student_id INT NOT NULL,
        date DATE NOT NULL,
        status VARCHAR(255) NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        FOREIGN KEY (student_id) REFERENCES students(student_id)
    )`;
    
    const createAccessTokenTableQuery = `CREATE TABLE IF NOT EXISTS refresh_tokens(
        token_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        token VARCHAR(255) NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    )`;

    const createResetTokenTableQuery = `CREATE TABLE IF NOT EXISTS password_reset_tokens (
        id INT AUTO_INCREMENT PRIMARY KEY,
        user_id INT,
        reset_token VARCHAR(255),
        reset_token_expires DATETIME,
        FOREIGN KEY (user_id) REFERENCES students(student_id) ON DELETE CASCADE
    )`;

    const QRidTableQuery = `CREATE TABLE IF NOT EXISTS qr_id (
        id INT AUTO_INCREMENT PRIMARY KEY,
        qr_id VARCHAR(255),
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
        )`;

    function executeQuery() {
        // Get the current date in MySQL date format (YYYY-MM-DD)
        const currentDate = new Date().toISOString().split('T')[0];

        // SQL query to insert records into the attendance table
        const query = `
            INSERT INTO attendance (student_id, date, status)
            SELECT student_id, '${currentDate}' as date, 'absent' as status
            FROM students
        `;
        // Execute the query
        db.query(query, (err, results) => {
            if (err) {
                console.error('Error executing query:', err.message);
            } else {
                console.log('Records inserted successfully.');
            }
        });
    }
    

    const TIME = '0 0 * * *'
    
    const job = new cron.schedule(TIME, executeQuery,{
        scheduled: true,
        timezone: 'Asia/Kolkata'
    })

    job.start();

    db.query(createStudentTableQuery, (error) => {
        if (error) throw error;
        console.log('Student table created or already exists');
    });
    db.query(createAdminTableQuery, (error) => {
        if (error) throw error;
        console.log('Admin table created or already exists');
    });
    db.query(createAttendanceTableQuery, (error) => {
        if (error) throw error;
        console.log('Attendance table created or already exists');
    });
    db.query(createAccessTokenTableQuery, (error) => {
        if (error) throw error;
        console.log('Access Token table created or already exists');
    });
    db.query(createResetTokenTableQuery, (error) => {
        if (error) throw error;
        console.log('Reset Token table created or already exists');
    });
    db.query(QRidTableQuery, (error) => {
        if (error) throw error;
        console.log('QR id table created or already exists');
    });
});

module.exports = db;
const db = require("../database/db")

exports.totalAttendanceOfStudent = (req, res) =>{

    const {student_id} = req.body
    

    const totalAttendanceQuery = `
        SELECT COUNT(*) AS total_attendance
        FROM attendance
        WHERE student_id = ?
        `;
        db.query(totalAttendanceQuery, [student_id], (error, results) => {
            if (error) {
                console.error('Error fetching total attendance:', error);
                res.status(500).json({ error: 'Internal Server Error' });
            } else {
                const totalAttendance = results[0].total_attendance;
                res.status(200).json({ totalAttendance });
            }
        });
}

exports.student_present = async(req, res) => {

    const {student_id} = req.body

    const countPresentQuery = `
        SELECT COUNT(*) AS total_present
        FROM attendance
        WHERE student_id = ? AND status = 'present'
        `;
        db.query(countPresentQuery, [student_id], (error, results) => {
            if (error) {
                console.error('Error fetching total attendance:', error);
                res.status(500).json({ error: 'Internal Server Error' });
            } else {
                const totalPresent = results[0].total_present;
                res.status(200).json({ totalPresent });
            }
        });
}

exports.student_absent = async(req, res) => {

    const {student_id} = req.body

    const countAbsentQuery = `
        SELECT COUNT(*) AS total_absent
        FROM attendance
        WHERE student_id = ? AND status = 'absent'
        `;
    db.query(countAbsentQuery, [student_id], (error, results) => {
        if (error) {
            console.error('Error fetching total attendance:', error);
            res.status(500).json({ error: 'Internal Server Error' });
        } else {
            const totalAbsent = results[0].total_absent;
            res.status(200).json({ totalAbsent });
        }
    });
}
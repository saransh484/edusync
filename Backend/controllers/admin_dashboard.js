const db = require("../database/db")

exports.total_students = async (req, res) => {

    const totalStudentsQuery = `
        SELECT COUNT(*) AS total_students
        FROM students
        `;

    db.query(totalStudentsQuery, (error, results) => {
        if (error) {
            console.error('Error fetching total students:', error);
            res.status(500).json({ error: 'Internal Server Error' });
        } else {
            const totalStudents = results[0].total_students;
            res.status(200).json({ totalStudents });
        }
    });
}

exports.attendance = async (req, res) => {
    const { student_id, qr_id } = req.body;
    const date = new Date(); 
    
    const checkQRCode = `
        SELECT * FROM qr_id WHERE qr_id = ?`
    
    db.query(checkQRCode, [qr_id], (err, result) => {
        if (err) {
            console.error('Error checking QR code:', err);
            res.status(500).json({ error: 'Internal Server Error' });
        }
        else {
            if (result.length === 0) {
                res.status(404).json({ error: 'QR code not found' });
            }
            else {
                const insertAttendanceQuery = `
                UPDATE attendance
                SET status = 'present'
                WHERE student_id = ?
                    `;
                    try {
                        db.query(insertAttendanceQuery, [student_id], (error, results) => {
                            if (error) {
                                console.error('Error inserting attendance:', error);
                                res.status(500).json({ error: 'Internal Server Error' });
                            } else {
                                res.status(200).json({ success: true });
                            }
                        });
                    } catch (error) {
                        console.error('Error inserting attendance:', error);
                        res.status(500).json({ error: 'Internal Server Error' });
                    }
            }
        }
    })


}

exports.total_present = async (req, res) => {

    const { date } = req.body

    const count_present = `SELECT * FROM attendance WHERE status = 'present' AND DATE(date) = ?`;
    db.query(count_present,[date], (error, results) => {
        if (error) {
            console.error('Error counting present:', error);
            res.status(500).json({ error: 'Internal Server Error' });
        }
        else {
            const present = results;
            const count = results.length
            
            res.status(200).json({ count, present });
        }
    });

}

exports.total_absent = async (req, res) => {

    const { date } = req.body;

    const count_absent = `SELECT * FROM attendance WHERE status = 'absent' AND DATE(date) = ?`;
    db.query(count_absent,[date], (error, results) => {
        if (error) {
            console.error('Error counting absent:', error);
            res.status(500).json({ error: 'Internal Server Error' });
        }
        else {
            const absent = results;
            const count = results.length
            res.status(200).json({ count, absent });
        }
    });

}

exports.allStudents = (req, res) => {
    const allStudentsQuery = `
        SELECT * FROM students
        `;
        db.query(allStudentsQuery, (error, results) => {
            if (error) {
                console.error('Error fetching all students:', error);
                res.status(500).json({ error: 'Internal Server Error' });
            } else {
                res.status(200).json({ results });
            }
        });
}

// ----- Student Management ------
exports.updateStudent = (req, res) => {
    const { student_id, name, email, mobile_number } = req.body;
    const updateStudentQuery = `
        UPDATE students
        SET name = ?, email = ?, mobile_number = ?
        WHERE student_id = ?
        `;
        db.query(updateStudentQuery, [name, email, mobile_number, student_id], (error, results) => {
            if (error) {
                console.error('Error updating student:', error);
                res.status(500).json({ error: 'Internal Server Error' });
            } else {
                res.status(200).json({ success: true });
            }
        });
}

exports.deleteStudent = (req, res) => {
    const { student_id } = req.body;
    const deleteStudentQuery = `
        DELETE FROM students
        WHERE student_id = ?
        `;
        db.query(deleteStudentQuery, [student_id], (error, results) => {
            if (error) {
                console.error('Error deleting student:', error);
                res.status(500).json({ error: 'Internal Server Error' });
            } else {
                res.status(200).json({ success: true });
            }
        });
}

exports.searchStudent = (req, res) => {
    const { student_id } = req.body;
    const searchStudentQuery = `
        SELECT * FROM students
        WHERE student_id = ?
        `;
        db.query(searchStudentQuery, [student_id], (error, results) => {
            if (error) {
                console.error('Error searching student:', error);
                res.status(500).json({ error: 'Internal Server Error' });
            } else {
                res.status(200).json({ results });
            }
        });
}

exports.dayAttendees = async(req, res) => {
    const {date} = req.body;

    const dayAttendeesQuery = `
        SELECT
            students.name,
            attendance.date,
            attendance.status,
            attendance.student_id
        FROM
            students
        JOIN
            attendance ON students.student_id = attendance.student_id
        WHERE
            attendance.date = ?
        `;
        db.query(dayAttendeesQuery, [date], (error, results) => {
            if (error) {
                console.error('Error fetching day attendees:', error);
                res.status(500).json({ error: 'Internal Server Error' });
            } else {
                res.status(200).json({ results });
            }
        });
}

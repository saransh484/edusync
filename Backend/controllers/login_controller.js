const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const db = require("../database/db")
const uuid = require("uuid")
const nodemailer = require('nodemailer')

exports.test = async (req, res)=>{
    res.send("working")
}

// deprecated
exports.admin_signup = async (req,res)=>{
    const {
        admin_id,
        name,
        email,
        mobile_number,
        password
    } = req.body

    const insertAdminQuery = `
        INSERT INTO admin (admin_id, name, email, mobile_number, hashed_password)
        VALUES (?, ?, ?, ?, ?)
        `;

    try {
        await db.query(insertAdminQuery, [
            admin_id,
            name,
            email,
            mobile_number,
            await bcrypt.hash(password, 10)
        ]);
        res.send("success")
        console.log("new admin entry added successfully")

    } catch (error) {
        res.send(error)
    }
}

exports.student_signup = async (req, res) => {

    const {
        student_id,
        name,
        email,
        mobile_number,
        password
    } = req.body

    const insertStudentQuery = `
        INSERT INTO students (student_id, name, email, mobile_number, hashed_password)
        VALUES (?, ?, ?, ?, ?)
        `;

    const checkEmailExistsQuery = `
        SELECT *
        FROM students
        WHERE student_id = ? AND email = ?
        `;
    try {
        db.query(checkEmailExistsQuery, [student_id, email], async (error, results) => {
            if (error) {
                console.error('Error checking email existence:', error);
                res.status(500).json({ error: 'Internal Server Error' });
            } else {
                if (results.length > 0) {
                // Email already exists, cannot sign up
                res.status(400).json({ error: 'Email and ID already in use' });
                } else {
                // Email does not exist, proceed with sign-up
                await db.query(insertStudentQuery, [
                    student_id,
                    name,
                    email,
                    mobile_number,
                    await bcrypt.hash(password, 10)
                ], (error, results) => {
                    if (error) {
                    console.error('Error inserting student:', error);
                    res.status(500).json({ error: 'Internal Server Error' });
                    } else {
                    res.send("success")
                    console.log("new student entry added successfully")
                    }
                });
                const currentDate = new Date().toISOString().split('T')[0];
                const query = `
                    INSERT INTO attendance (student_id, date, status)
                    VALUES (?, ?, ?)`;
                // Execute the query
                db.query(query,[student_id,currentDate,'absent'], (err, results) => {
                    if (err) {
                        console.error('Error executing query:', err.message);
                    } else {
                        console.log('Records inserted successfully.');
                    }
                });
                }
            }})

    } catch (error) {
        res.send(error)
    }
}

exports.student_login = async (req, res) => {

    const { email, password } = req.body

    const checkStudentQuery = `SELECT * FROM students WHERE email = ?`;

    db.query(checkStudentQuery, [email], (error, results) => {
        if (error) {
            console.error('Error checking student credentials:', error);
        } else {
            if (results.length > 0) {
            const StudentInfo = results[0];

            // Use bcrypt to compare the provided password with the hashed password in the database
            bcrypt.compare(password, StudentInfo.hashed_password, async (compareError, isMatch) => {
                if (compareError) {
                console.error('Error comparing passwords:', compareError);
                } else {
                if (isMatch) {
                  // Passwords match, login successful
                    const token = jwt.sign({ userID: StudentInfo.student_id }, process.env.JWT_SECRET, {
                    expiresIn: '15m',
                    });

                     // Generate a refresh token and store it in the database
                    const refreshToken = uuid.v4();
                    await db.query('INSERT INTO refresh_tokens (token) VALUES (?)', [refreshToken]);

                    res.cookie('refresh_token', refreshToken, {
                        httpOnly: true,
                        sameSite: 'None', 
                        secure: true,
                        maxAge: 24*60*60*1000
                    });
                    res.status(200).json({ accessToken: token, userID: StudentInfo.student_id, name: StudentInfo.name});

                    console.log('Login successful');
                    console.log('student ID:', StudentInfo.student_id);
                    console.log('student Name:', StudentInfo.name);
                } else {
                  // Passwords do not match
                    console.log('Invalid password');
                    res.send("Invalid password")
                }}
            });
        } else {
            // No admin found with the provided email
            console.log('Invalid email');
            res.send("Invalid email")
        }
        }})

}


// Route to refresh the access token using a refresh token
exports.refresh =  async (req, res) => {

    if (req.cookies?.refresh_token) {
        
        const refreshToken = req.cookies.refresh_token;

        try {
            // Check if the refresh token exists in the database
            db.query('SELECT * FROM refresh_tokens WHERE token = ?', [refreshToken], async (error, result) => {
                if (error) {
                    console.error('Error checking refresh token:', error);
              //   return res.status(500).json({ error: 'Internal Server Error' });
            }
            console.log(result)
            if (result.length === 0) {
                return res.status(401).json({ error: 'Invalid refresh token' });
            }
            else{
                await db.query('DELETE FROM refresh_tokens WHERE token = ?', [refreshToken]);

                   // Generate a new access token
                const user = req.user;
                const newAccessToken = jwt.sign({userID: user}, process.env.JWT_SECRET, { expiresIn: '12h' });
                res.json({ accessToken: newAccessToken });
            }
        });
        } catch (error) {
            console.error('Error refreshing token:', error);
            res.status(500).json({ error: 'Internal Server Error' });
        }

    }
    else{
        return res.status(401).json({ error: 'Invalid refresh token' });
    }

    
};

exports.admin_login = async (req, res) => {

    const { email, password } = req.body

    const checkAdminQuery = `SELECT * FROM admin WHERE email = ?`;

    db.query(checkAdminQuery, [email], (error, results) => {
        if (error) {
            console.error('Error checking student credentials:', error);
        } else {
            if (results.length > 0) {
            const AdminInfo = results[0];

            // Use bcrypt to compare the provided password with the hashed password in the database
            bcrypt.compare(password, AdminInfo.hashed_password, (compareError, isMatch) => {
                if (compareError) {
                console.error('Error comparing passwords:', compareError);
                } else {
                if (isMatch) {
                  // Passwords match, login successful
                    const token = jwt.sign({ userID: AdminInfo.admin_id }, process.env.JWT_SECRET2, {
                    expiresIn: '1h',
                    });
                    res.status(200).json({ token, userID: AdminInfo.admin_id, name: AdminInfo.name});

                    console.log('Login successful');
                    console.log('Admin ID:', AdminInfo.admin_id);
                    console.log('Admin Name:', AdminInfo.name);
                } else {
                  // Passwords do not match
                    console.log('Invalid password');
                    res.send("Invalid password")
                }}
            });
        } else {
            // No admin found with the provided email
            console.log('Invalid email');
            res.send("Invalid email")
        }
        }})
}


const transporter = nodemailer.createTransport({
    service:'gmail',
    debug: true,
    auth: {
        user: process.env.EMAIL,
        pass: process.env.EMAIL_PASSWORD
    }
});

exports.resetPasswordRequest = async (req, res) =>{

    const {email} = req.body;

    const resetToken = uuid.v4();

    try {
        // Save the reset token and expiration time in the database
        const find_id = 'SELECT student_id FROM students WHERE email = ?';
        db.query(find_id, [email], async (error, result) => {
            if (error) {
            console.error('Error finding user:', error);
            res.status(500).json({ error: 'Internal Server Error' });
            return;
            }
            console.log(result[0].student_id)
            const query = 'INSERT INTO password_reset_tokens (user_id, reset_token, reset_token_expires) VALUES (?, ?, ?)';
            db.query(query, [result[0].student_id, resetToken, new Date(Date.now() + 3600000)], (error, result) => {
            if (error || result.affectedRows === 0) {
                return res.status(404).json({ error: 'User not found' });
            }
          // Send the reset link to the user's email
            const resetLink = `https://flipr-task.vercel.app/reset-password?token=${resetToken}`;
            const mailOptions = {
            from: "smaranxoxo@gmail.com",
            to: email,
            subject: 'Password Reset Request',
            text: `Click the following link to reset your password ${resetLink}`,
        }
        
        transporter.sendMail(mailOptions, (mailError) => {
            if (mailError) {
                console.error('Error sending password reset email:', mailError);
                res.status(500).json({ error: 'Internal Server Error' });
            } else {
                res.json({ message: 'Password reset link sent successfully' });
            }
        });
        })});
    } catch (error) {
        console.error('Error requesting password reset:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
}

exports.resetPassword = async (req, res) => {
    const { newPassword, token } = req.body;

    const NewPassword = await bcrypt.hash(newPassword, 10)
    console.log("got :",token)

    try {
    // Verify the token and check expiration time
    const query = 'SELECT user_id FROM password_reset_tokens WHERE reset_token = ?';
    db.query(query, [token], (selectError, result) => {
        if (selectError || result.length === 0) {
        return res.status(401).json({ error: 'Invalid or expired token' });
        }

        const userId = result[0].user_id;

      // Update the user's password and clear the reset token
        const updateQuery = 'UPDATE students SET hashed_password = ? WHERE student_id = ?';
        db.query(updateQuery, [NewPassword, userId], (updateError) => {
        if (updateError) {
            console.error('Error updating password:', updateError);
            res.status(500).json({ error: 'Internal Server Error' });
        } else {
            const deleteQuery = 'DELETE FROM password_reset_tokens WHERE reset_token = ?';
            db.query(deleteQuery, [token], (deleteError) => {
            if (deleteError) {
                console.error('Error deleting reset token:', deleteError);
            }
        });

        res.status(200).json({ message: 'Password reset successfully' });
        }
        });
    });
    } catch (error) {
    console.error('Error resetting password:', error);
    res.status(500).json({ error: 'Internal Server Error' });
    }
}
const express = require('express');
const cors = require('cors');
const http = require('http');
const cookieParser = require('cookie-parser');
const app = express();
const helmet = require('helmet');

require('dotenv').config();

const port = process.env.PORT || 5000;

const corsOptions = {
    origin: '*',
    methods: ['GET', 'POST', 'OPTIONS'],
    allowedHeaders: ['Content-Type'],
    credentials: true,
    optionSuccessStatus: 204,
};

app.use(cors(corsOptions));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); 
    res.header("Access-Control-Allow-Headers", "*");
    res.header("Access-Control-Allow-Methods", "*");
    next();
});

app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// ---- route ----
app.use('/api', require('./routes/Main_route'));
app.use('/auth', require('./routes/auth_routes'));

const server = http.createServer(app);

// Start WebSocket server
setTimeout(() => {
    const startWebSocketServer = require('./middleware/websocket'); 
    startWebSocketServer(server);
}, 2000);

server.listen(port, () => {
    console.log(`🚀 Server is running on port ${port}`);
});

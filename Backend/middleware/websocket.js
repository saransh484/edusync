const WebSocket = require('ws');
const { v4: uuidv4 } = require('uuid');
const db = require("../database/db");

db.query("DELETE FROM qr_id", (error)=>{
    if(error) console.log(error)
})


function startWebSocketServer(server) {
    const wss = new WebSocket.Server({ server });
    const clients = new Set();
    
    wss.on('connection', (ws) => {
        console.log('WebSocket client connected');
        clients.add(ws);
        generateUUID();
        var interval =  setInterval(generateUUID, 60000);

        conn = true;
        
        function generateUUID() {

            const newUUID = uuidv4();
            // console.log(newUUID);
            const UUIDquery = `
                INSERT INTO qr_id (qr_id)
                VALUES (?)
                `;
            db.query(UUIDquery, [newUUID],(error)=>{
                if(error) console.log(error)
            });
            broadcast(newUUID);
            setTimeout(() => {
                const deleteUUID = `
                    DELETE FROM qr_id
                    WHERE qr_id = ?;
                    `;
                db.query(deleteUUID, [newUUID],(error)=>{
                    if(error) console.log(error)
                });
                
            },60000)
        
    }



    ws.on('message', (message) => {
        console.log(`Received: ${message}`);
    });
    

    ws.on('close', () => {
        console.log('WebSocket client disconnected');
        clients.delete(ws);
        db.query("DELETE FROM qr_id", (error)=>{
            if(error) console.log(error)
        })
    
        clearInterval(interval);
        conn = false;
    });

    if (conn) {
        console.log('WebSocket connection established');
        // setInterval(generateUUID, 60000);
    }    

});


function broadcast(message) {
    clients.forEach((client) => {
        if (client.readyState === WebSocket.OPEN) {
        client.send(message);
        }
    });
    }
}



module.exports = startWebSocketServer;
